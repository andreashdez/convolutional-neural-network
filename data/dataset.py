
class Dataset:
    """Store details of the loaded dataset."""

    def __init__(self, nb_classes, img_rows, img_cols, img_channels,
                 validated_classes,
                 train_data, train_labels,
                 validate_data, validate_labels,
                 test_data, test_labels):
        self.nb_classes = nb_classes
        self.img_rows = img_rows
        self.img_cols = img_cols
        self.img_channels = img_channels
        self.validated_classes = validated_classes
        self.train_data = train_data
        self.train_labels = train_labels
        self.validate_data = validate_data
        self.validate_labels = validate_labels
        self.test_data = test_data
        self.test_labels = test_labels

    def train_data_size(self):
        return self.train_data.shape[0]
