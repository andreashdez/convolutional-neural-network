
from data.dataset import Dataset
from keras.datasets import cifar10
from keras.utils import np_utils
from matplotlib import colors
import numpy as np
from sklearn.cross_validation import train_test_split


def load_data(model_name, grey_scale=False):
    """
    Load data using pre-configured dataset loader within keras.

    MNIST (http://yann.lecun.com/exdb/mnist/)
        - mnist.load_data()
    CIFAR10 (https://www.cs.toronto.edu/~kriz/cifar.html)
        - cifar10.load_data()
    CIFAR100 (https://www.cs.toronto.edu/~kriz/cifar.html)
        - cifar100.load_data()
    """
    # The data, shuffled and split between train and test sets
    (train_data, train_labels), (test_data, test_labels) = cifar10.load_data()

    train_data = train_data.astype("float32") / 255
    test_data = test_data.astype("float32") / 255

    if grey_scale:
        train_data = rgb_to_greyscale(train_data)
        test_data = rgb_to_greyscale(test_data)

    # Number of classes in the used dataset
    nb_classes = 10
    # input image dimensions
    img_rows, img_cols = train_data.shape[2], train_data.shape[3]
    # RGB images have 3 channels and greyscale images have 1
    img_channels = train_data.shape[1]

    # MLPs need a 1-dimensional arrar
    if model_name == "mlp":
        train_data = train_data.reshape(train_data.shape[0],
                                        (train_data.shape[1]
                                         * train_data.shape[2]
                                         * train_data.shape[3]))
        test_data = test_data.reshape(test_data.shape[0],
                                      (test_data.shape[1]
                                       * test_data.shape[2]
                                       * test_data.shape[3]))
    else:
        train_data = train_data.astype("float32")
        test_data = test_data.astype("float32")

    train_data, val_data, train_labels, val_labels = train_test_split(
        train_data, train_labels, test_size=0.2, random_state=88)

    # create flat numpy array of test labels for evaluation of the model
    validated_classes = test_labels.reshape(test_labels.shape[0])

    # convert class vectors to binary class matrices
    train_labels = np_utils.to_categorical(train_labels, nb_classes)
    val_labels = np_utils.to_categorical(val_labels, nb_classes)
    test_labels = np_utils.to_categorical(test_labels, nb_classes)

    dataset = Dataset(nb_classes, img_rows, img_cols, img_channels,
                      validated_classes,
                      train_data, train_labels,
                      val_data, val_labels,
                      test_data, test_labels)
    return dataset


def rgb_to_greyscale(dataset_rgb):
    """
    Convert each image in the given dataset to greyscale.

    The dataset used in the model uses this specific shape:
        [channels, hight, width]
    it has to be changed as the rgb_to_hsv function needs this shape:
        [hight, width, channels]
    The new greyscale image is stored in a new array only using the last
    value of the hsv array.
    This new array has to be reshaped to meet the original criteria
    of the model.
    """
    dataset_grey = np.zeros((dataset_rgb.shape[0], 1,
                             dataset_rgb.shape[2], dataset_rgb.shape[3]))

    for i in range(len(dataset_rgb)):
        img_rgb = np.swapaxes(dataset_rgb[i], 0, 2)
        img_hsv = colors.rgb_to_hsv(img_rgb)
        img_grey = np.zeros((img_hsv.shape[0], img_hsv.shape[1]))
        for x in range(len(img_hsv)):
            for y in range(len(img_hsv[x])):
                # Only need the last of the three values
                img_grey[x][y] = img_hsv[x][y][2:]
        img_grey = img_grey.reshape(32, 32, 1)
        img_grey = np.swapaxes(img_grey, 2, 0)
        dataset_grey[i] = img_grey

    return dataset_grey
