
from keras.models import model_from_json


def save_model(model, model_name="model"):
    print("Saving model as \"%s\"" % model_name)
    json_model = model.to_json()
    open("models/" + model_name + ".json", "w").write(json_model)
    model.save_weights("models/" + model_name + ".h5")


def load_model(model_name, training=False):
    print("Loading model \"%s\"" % model_name)
    model = model_from_json(open("models/" + model_name + ".json").read())
    if training:
        model.load_weights("models/" + model_name + ".h5")
    else:
        model.load_weights("best_weights/" + model_name + ".h5")
    return model
