
import class_types as ct
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix, classification_report


def evaluate_model(model, dataset):
    """
    Evaluate convolutional neural network model.

    Evaluate on the test data which has not been used in the training process.
    Creates a confusion matrix and a classification report
    to aid evaluation.
    """
    print("\n- EVALUATING MODEL ---------------------------------------------")
    score = model.evaluate(dataset.test_data, dataset.test_labels,
                           verbose=1)
    print(score)

    predicted_classes = model.predict_classes(dataset.test_data,
                                              verbose=0).tolist()

    print("\nConfusion matrix:")
    conf_matrix = confusion_matrix(dataset.validated_classes,
                                   predicted_classes)
    print(conf_matrix)
    plot_confusion_matrix(conf_matrix)

    print("\nClassification report:")
    print(classification_report(dataset.validated_classes, predicted_classes,
                                target_names=ct.class_names))


def plot_confusion_matrix(conf_matrix):
    plt.imshow(conf_matrix, interpolation="nearest")
    plt.colorbar()
    tick_marks = np.arange(len(ct.class_names))
    plt.xticks(tick_marks, ct.class_names, rotation=45)
    plt.yticks(tick_marks, ct.class_names)
    plt.tight_layout()
    plt.show()


def plot_learning_rate(hist):
    plt.plot(hist.history["loss"], "b-", label="training loss")
    plt.plot(hist.history["val_loss"], "g-", label="validation loss")
    plt.legend()
    plt.show()
