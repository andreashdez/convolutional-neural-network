\chapter{Background \& Objectives}

In this chapter we will introduce the main concepts of machine learning relevant to artificial neural networks and how they have added up to make models that recognise objects in images, an introduction to the dataset used for training the network and briefly touching the project life cycle and the research methodology.

\section{Background}

Neural networks are extremely complex constructs found in our brains, capable of doing remarkable tasks. This network contains an incredibly large number of neurons that, when looked at on their own, are not so complex.

These networks are never pre-programmed, although a small bias could be possible. They learn from experience on their own or with outside help. This process of learning/training can be very time-consuming and resource intensive.

It was not until recently, that scientists have slowly begun to understand how such a vast network of simple neurons makes us think and be who we are.

\subsection{Neural networks}

Artificial neural networks (ANNs) are computational processing systems inspired by biological neural networks and can be very advantageous for solving tasks with a large number of inputs. Just like their biological counterpart, artificial neural networks are comprised of interconnected ``neurons'' (computational units) that communicate with each other. Each connection has a numeric weight that can be fine-tuned by training the network with pre-existent data.

The ability to learn from experience makes neural networks more suitable than standard rule-based programming at solving complex problems. They don't have to rely on rules being made up by programmers beforehand to cope with each possible scenario they might encounter. This makes artificial neural networks very flexible and useful for a wide variety of problems.

On the other hand, artificial neural networks require a lot of training to be able to make good predictions. Training is very dependent on the quality as well as the quantity of the provided data. It also has a very high demand of hardware resources in order to process the high quantities of data.

Artificial neural networks are not a new concept. Many important algorithms have been created in the past century, including the perceptron (computational unit of the network) and backpropagation (to train the network by updating its weights). The networks used were very shallow, only containing up to two layers of neurons and therefore had a limited performance. This lead the machine learning community to start favouring other classification methods.

In 1943 Warren McCulloch and Walter Pitts created the first computational model for neural networks in general \cite{citeulike:1295398}. This was the main contribution for a lot of research to be done in this field, on one side was the research of the biological neural networks and on the other was the application of these ideas for artificial intelligence.

Also in the 1940s a psychologist named Donald Hebb proposed the idea of learning with the mechanism of neural plasticity. This is now known as Hebbian learning and considered as the typical unsupervised learning rule.

In 1958 Frank Rosenblatt created the perceptron, a simple component in a two-layer computer learning network \cite{citeulike:13697582}. In 1969 Marvin Minsky and Seymour Papert made a publication that pointed out two main problems of neural networks. The simple perceptron used was not capable of solving the exclusive-or gate and the hardware of the time was not powerful enough for the resource intensive learning of neural networks.

In 1975 a very important contribution was made by Werbos called backpropagation. This new method allows multiple layered neural networks to be trained more efficiently, as well as now with the added layers being able to solve the exclusive-or gate problem.

In recent years deep-learning techniques have improved the performance of neural networks. Deep neural networks (DNNs) are ANNs with multiple hidden layers between their input and output layers \cite{citeulike:4196818}. The big majority of the DNNs are feedforward neural networks, meaning that the information moves only forward from the input to the output. This makes it easier to compute an output on the already complex deep networks.

Convolutional neural networks (CNNs) are a type of deep neural network. This model has shown to be especially good at processing 2D structured inputs, useful in image and speech recognition. It was inspired by the organization of the animal visual cortex. The network is arranged so that each individual neuron responds to an overlapping region of the incoming information.

\subsection{Supervised learning}

Artificial neural networks usually get trained with a method called supervised learning. It describes the learning of a task providing examples, as in labelled data with which one can immediately check if the outcome is desired or not. As opposed to unsupervised learning we already know the target and just have to ``move'' in the right direction to get to it.

Problems as pattern recognition and regression are usually supervised learning tasks. In this project we are doing patter recognition, also known as classification, and therefore will be using supervised learning providing a large, labelled dataset of images. It should be noted, that there has been research done about un-supervised learning to initialise the neural network \cite{citeulike:8819209}.

\subsection{CUDA}

Graphical processing units (GPUs) have been built with a highly parallel structure to cope with compute-intensive tasks of real-time graphics. CUDA is a platform and application programming interface (API) created by NVIDIA that enables the use of CUDA-enabled GPUs for general purpose processing, often referred to as general purpose graphical processing unit (GPGPU).

In order to train a neural network with a large image dataset it would be very advantageous to make use of the advantages of parallel processing provided by GPUs. Compared to other platforms, such as OpenACC and OpenCL, CUDA is the best maintained and supported framework currently in use.

\subsection{Datasets}

In order to make the neural networks predict image classes we need data for them to be trained on. A lot of research has been going into image recognition lately and many image datasets have been created for this purpose.

A widely used image dataset is the CIFAR-10, containing 60000 colour images with a small of dimension 32 times 32 pixels. This dataset is made up of 10 different classes, each mutually exclusive to make it easier for the neural network to train on.

The advantage of using this dataset is that it has been used in a lot of different models for research and even in competitions. Many results have been collected that can be used for comparison.

The project cuda-convnet has built a fast C++/CUDA implementation of a convolutional neural network (making use of parallel processing of a GPU). This model manages an error rate of 18\% without any data augmentation and an error rate of 11\% with.

\subsection{Technical preparation}

There are many software libraries that aid the implementation of neural networks, many of which are built with Python. It is also important to be able to access the multi-processing capabilities of a graphical processing unit (GPU). This speeds up training of a neural network considerably.

For this project, installing the CUDA framework was vital. On an nVidia GPU, which is CUDA enabled, this means that the multi-processing capabilities can be used for general purpose. There are also a number of deep learning frameworks that make use of CUDA. A library that works on top of CUDA is the cuDNN, it gives us optimisations for performance and lets us reserve memory for the training process of neural networks\cite{citeulike:14026268}.

A few frameworks have made a name for themselves when it comes to building neural networks, some notable ones are Caffe, Torch, Theano and TensorFlow, both are open source. At the time of writing this document TensorFlow is still in early development and not as stable as Theano. This made the choice easy, considering that there were no real differences feature wise.

On top of Theano there are a number of software libraries that can be run to make the creation of neural networks easier, including Lasagne, Keras \cite{chollet2015keras}, Pylearn2 and others. For this project Keras has been chosen as it seemed straight forward to install, set up and use.

\section{Analysis}

The aim of this project is to investigate the application of convolutional neural networks for image classification using a large benchmarking dataset. The networks will be trained on the CIFAR-10 dataset, which means that they should be capable of classifying images for the ten given classes. As this dataset is so popular, it also makes it easier to compare the models built for this project to other models.

\begin{itemize}
    \item The system should be capable of scoring a decent accuracy. It should be capable of classifying correctly more than two thirds of the provided dataset. It does not need to be comparable to the top best models found by other researchers.
    \item A number of neural networks will be created for the system and the user should be able to specify which one to use. This feature will also be used when setting up experiments to compare the different models.
    \item Experimentation with data augmentation should be possible. This experimentation should be able to find if pre-processing of data helps prevent overfitting and aids accuracy.
    \item Experimentation of accuracy of convolutional neural networks. The system should evaluate the created models and provide helpful information to validate its accuracy.
    \item The system should be able to recognize new images provided by the user and classify them, provided they contain one of the objects belonging to the ten classes that the network was trained on. In case that the dimensions of the provided image do not match the ones of the network it should be able to resize them.
\end{itemize}

\section{Research method}

The project uses quantitative research to analyse the effectiveness of convolutional neural networks. A number of different models has been produced, each of which have a big number of variables that can be tweaked. This will generate a large number of results that can be compared and used for statistical analysis.

\section{Project lifecycle}

At first sight agile methodologies seem to be the most appropriate to develop a project of this type. They tend to be more suited for tackling the unknown as opposed to plan driven development methodologies. But there are certain aspects of agile methodologies that cannot be met in this project.

First of all, this is a solo project, which makes pair programming not possible. Pair programming is one of the key components of agile methodologies, to be able to exchange ideas and not get stuck on small problems.

There is also the idea in agile methodologies, that the customer plays a vital part in the development of the software. In this project there is no real customer, although it could be noted that the actual developer is also the client. The project does not only develop software, but also uses it to do some research. The research is done by the same person developing the software and therefore these two ``personas'' are working together on the development.

The development of this project will follow certain aspects of agile methodologies, as there are many helpful techniques that would make the development run smoother. Especially concepts like refactoring and simplicity in design are going to be vital for this project.

Planned driven methodologies require the whole system to be design before any development begins. This might work well when the development team has previous experience with the task at hand, but in this case it is the first time the developer is undergoing such a project. Therefore, it would be more advantageous if the developer gains the experience at the same time as development is happening.

Version control is another useful component often used in agile methodologies. This will be a very useful tool in this project, it is easy to set up and very helpful at backing up important files. There is also a psychological aspect to version controlling, it helps finishing certain features of the project to be able to upload them as a new version of the system.

A GitLab repository has been created to host the project, initially chosen as it provides private project hosting for free (without the need of a student account), although this project has been made public.

GitLab page: https://gitlab.com/andreashdez/convolutional-neural-network
