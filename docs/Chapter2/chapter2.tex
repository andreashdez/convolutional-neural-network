\chapter{Methodology}

\section{Image dataset}

This project makes use of the CIFAR-10 image dataset to train and evaluate a number of different neural network models. It consists of 60,000 small coloured images with a dimension of 32 by 32 pixels. Each image will be composed of 3 layers, one for each colour channel red, green and blue (RGB), of 32 times 32 pixels, with a total of 3,072 inputs to the neural network.

The data set is comprised of 10 different image classes with an equal number of images in each class. The classes are airplane, automobile, bird, cat, deer, dog, frog, horse, ship and truck. It has been made sure that there is no overlap between the classes, for example automobiles do not contain pickup trucks that could be misclassified as trucks. The trucks class only contains big trucks.

The image set is split in five training sets of each 10,000 images and one testing set with the remaining 10,000 images. The testing set has an equal number of images (1,000) for each of the 10 classes. The remaining five training sets contain images randomly, without having an equal split of classes.

\section{Artificial neurons}

Artificial neurons are modelled after their biological counterpart, as seen in the diagram below they take a number of inputs (signals through dendrites) and produce one output signal (along its axon).

\begin{figure}[H]
    \centering
        \includegraphics[width=100mm]{Images/biological_neuron.png}
        \caption{Drawing of a biological neuron \cite{stanfordCNN}.}
\end{figure}

\begin{figure}[H]
    \centering
        \includegraphics[width=80mm]{Images/mathematical_neuron_model.jpeg}
        \caption{Drawing of the mathematical model representing a biological neuron \cite{stanfordCNN}.}
\end{figure}

The output of the neuron will be one of the inputs to the next neuron. Depending on the strength of this signal, it will be multiplied by the weight of the respective input of the next neuron.In the image above, we can see the inputs x1, x2 and x3 are multiplied by the respective weights w1, w2 and w3. The result will go into the cell body and be summed up to its bias. All the values from the inputs will be summed together and given to the activation function and then be output to the next neuron. The mathematical notation is shown below:

\begin{equation}
	output = f\left(\sum_i weight_i \times input_i + bias\right)
\end{equation}

\subsection{Activation functions}

The activation function is used in neural networks to eliminate its linearity; they are fixed mathematical operations which are performed on a single number on the neuron. There are many functions that can be used, the more common ones are explained in detail below. It has to be noted that recently, with the rise of deep neural networks, more simple activation functions are favoured as they are computationally less complex.

\subsubsection{Sigmoid activation function}

The sigmoid activation function will create a positive number between 0 and 1. Large negative numbers become 0 and large positive numbers become 1. Its mathematical form is the following:

\begin{equation}
    f(x) = \frac{1}{1 + e^{-x}}
\end{equation}

This function has been used frequently historically as it has a nice visual interpretation of a neuron giving a signal, from not signalling (0) for large negative values and giving a full signal (1) for large positive numbers.

\begin{figure}[H]
    \centering
        \includegraphics[width=70mm]{Images/sigmoid.jpeg}
        \caption{Curve of the sigmoid activation function \cite{stanfordCNN}.}
\end{figure}

The sigmoid activation function is not used frequently anymore, as it has two main disadvantages when applied to neural networks.

The sigmoid function kills gradients, it saturates quickly to either 0 or 1 which gives a gradient of almost 0. This will result in not giving a signal and losing the gradient when performing backpropagation. Large positive and negative numbers will always give the same result, with a non-existent gradient, and is therefore hard to train. This also applies to the initial weights applied to the neural network, it has to be made sure that they are not too big or small to not saturate the neurons.

The sigmoid function is not zero centred. This will be problematic in later stages of the training process of the neural network. Receiving not zero centred information could mean that it gets always positive data, which will influence the gradient during backpropagation, becoming either always positive or always negative.

\subsubsection{Hyperbolic tangent activation function}

The hyperbolic tangent activation function (TANH) is very similar to the sigmoid function. It will produce an output between -1 and 1, where large negative numbers become -1 and large positive numbers become 1. It can be noted with the following mathematical form:

\begin{equation}
    f(x) = \frac{e^x - e^{-x}}{e^x + e^{-x}}
\end{equation}

\begin{figure}[H]
    \centering
        \includegraphics[width=70mm]{Images/tanh.jpeg}
        \caption{Curve of the hyperbolic tangent activation function \cite{stanfordCNN}.}
\end{figure}

It still has the same disadvantage of saturating and killing the gradient, just like the sigmoid function, but it is favoured as it is zero centred.

\subsubsection{Step activation function}

The step activation function is the simplest non-linearity used for neural networks, as used by the original perceptron. The neuron will only be able to either emit a signal or not emit one. They are mainly used for binary classification, where the result has to be part of one of two classes.

\begin{figure}[H]
    \centering
        \includegraphics[width=50mm]{Images/step.png}
        \caption{Curve of the hyperbolic tangent activation function \cite{stanfordCNN}.}
\end{figure}

\subsubsection{Linear activation function}

The linear activation function is, as the name implies, linear. It does not have the non-linearity properties as the rest of the activation functions, which does not make it a good option for neural networks solving nontrivial problems.

\begin{figure}[H]
    \centering
        \includegraphics[width=50mm]{Images/linear.png}
        \caption{Curve of the linear activation function.}
\end{figure}

\subsubsection{Rectified linear unit (ReLU) activation function}

The rectified linear unit activation function, as the name implies, is very similar to the linear activation function. The only difference is that negative values are changed to 0. It was found that ReLU has a number of advantages compared to the other activation functions described in this document \cite{citeulike:13515098}.

On one side it seems to accelerate the convergence of stochastic gradient descent compared to the sigmoid and tanh functions. Also important to note is that it is a very simple operation and therefore is less resource intensive than other functions, which is especially important for deep neural networks with a very large number of neurons.

\begin{figure}[H]
    \centering
        \includegraphics[width=70mm]{Images/relu.jpeg}
        \caption{Curve of the ReLU activation function \cite{stanfordCNN}.}
\end{figure}

It does have one important disadvantage. As negative values are converted to zero, when a very large gradient is input into a ReLU neuron through backpropagation it could cause the weights to update in a way that most its inputs are negative. This would result in this neuron to not be activated by its inputs anymore and therefore not be updated through backpropagation. At this point it stops learning. This problem seems to be a big issue when the learning rate is set too high, with a good learning rate it could become less frequent.

\begin{equation}
    f(x) = max(0, x)
\end{equation}

\subsubsection{Leaky ReLU activation function}

The leaky ReLU activation function is an attempt to fix the disadvantage of the standard ReLU activation function. Instead of abruptly cutting off all values below zero to be equal to zero, a small negative linear slope is created. This can be noted mathematically with \(\alpha\) as a small constant as follows:

\begin{equation}
    f(x) = 1(x < 0)(\alpha x) + 1(x \geq 0)(x)
\end{equation}

\begin{figure}[H]
    \centering
        \includegraphics[width=70mm]{Images/leaky.jpeg}
        \caption{Curve of the Leaky ReLU activation function.}
\end{figure}

The success of this activation function is not yet proven, some improvements have been reported, but it is not always consistent. Some research is being done on improving this activation function using a randomized aspect \cite{citeulike:13901176}.

\section{Artificial neural network architecture}

This section will introduce the necessary building blocks to create an artificial neural network, able to take the selected images as input. It will explain the basic components used in multilayer perceptron models as well as in convolutional neural networks.

\subsection{Feedforward neural network}

A feedforward neural network as opposed to the recurrent neural network (RNN) has connections between neurons in a way that they do not form a cycle. This is the simplest form of artificial neural network and therefore easier to scale. RNNs have had issues as large networks could not be trained properly, although there is some research being done on them \cite{citeulike:13928900}. The following ANNs described are all feedforward neural networks.

\subsection{Artificial neural networks}

A neural network can be seen as a model of layers, each composed of a number of neurons. For example, we could have a 3-layer neural network that would be represented with the following formula:

\begin{equation}
    s = W_3 max(0, W_2 max(0, W_1 x))
\end{equation}

The layers are represented by \(W_3\), \(W_2\), and \(W_1\) and are parameters to be learned. The first layer could have for example 10 neurons, such creating an intermediate vector with dimension \([30720]\).

The function \(max(0, -)\) is an activation function applied to the element, in this case it’s the rectified linear unit function (ReLU) which simply changes all negative values to zero. This non-linearity is necessary as otherwise the matrixes could be collapsed together and the output be linearly dependent of the input. There are many activation functions as explained in more detail above in this document.

The output layer will have a loss function to be able to validate the network easier. This will then make it possible for the learning process to use optimization algorithms and techniques like backpropagation to adapt the parameters of each layer.

\begin{figure}[H]
    \centering
        \includegraphics[width=100mm]{Images/ann.jpeg}
        \caption{Artificial neural network with two hidden layers \cite{stanfordCNN}.}
\end{figure}

There are naming conventions for the number of layers that compose a network, the input layer is not included. When we have a model as seen above with the input layer, two hidden layers and the output layer, we say that we have a three-layer neural network.

\subsubsection{Neural network sizing}

It is important to understand how the size of a neural network affects its complexity as well as its possible results. It is always the case that, when we increase the amount of hidden neurons, the capacity of the neural network increases. It will be able to solve a larger amount of problems, as the increased amount of neurons can work together in more complex ways.

On the other hand, having an increased amount of neurons means that it is harder to train properly. More neurons can mean that they start fixating on the wrong features (noise) instead. This would be seen as overfitting, as it matches the training data perfectly but would not be able to classify testing data properly. Generally, larger networks are harder to generalize, but there are a number of methods to diminish the effects of overfitting which will be explained further on in this document.

To find how many parameters a neural network can learn, we have to count the total amount of weights and the biases together. Using the example neural network show in the image above, we can add up the number of neurons \(4 + 4 + 1 = 9\), each neuron with one bias gives us 9 biases. Counting the weights for the fully connected layers \([3 \times 4] + [4 \times 4] + [4 \times 1] = 32\). Adding the biases and the weights together, we get a total of 41 learnable parameters.

\subsubsection{Input layer}

In a standard neural network, the input layer of the network contains one neuron for each pixel. Using the RGB images provided by the CIFAR-10 dataset we would have 3 channels, resulting in an input vector with dimension \([3072]\).

\subsubsection{Output layer}

The output layer has no activation function, as it represents the class scores computed by the network. The classification is given in a one dimensional vector containing real-valued numbers or target for each class. In the CIFAR-10 dataset example we have 10 classes, therefore the vector will have a dimension of \([10]\). The output layer will make use of a softmax activation function for prediction as well as to bring down the cross-entropy loss. Recently, the idea to replace this with a support vector machine has come up \cite{citeulike:13617718}.

\subsection{Convolutional neural networks}

Convolutional neural networks are very similar to standard artificial neural networks. Just like ANNs, they are comprised of neurons, set up in layers, that take an input performing the dot product and adding their biases to it. Optionally they might also have an activation function before the output. At the end, the whole network will output a vector with the class scores as well as perform a loss function before the final output. The following section will explain the building blocks that make up the convolutional neural network.

\begin{figure}[H]
    \centering
        \includegraphics[width=100mm]{Images/cnn.jpeg}
        \caption{Input (red) going into convolutional neural network showing hidden layers (blue) and output layer (green) \cite{stanfordCNN}.}
\end{figure}

In the image above we can see how the three dimensional image (height, width, colour channels) is the input to the first layer. Each layer in the convolutional neural network is three dimensional, the depth representing the number of filters in each. The input will traverse the layer through its depth and the dot product is calculated at each neuron layer along the depth. The output will have been transformed through differentiable functions until it reaches the output layer. In the output layer the resulting vector will have only one dimension left, its depth, which will represent each class of the dataset.

\subsubsection{Input layer}

Convolutional neural networks are designed with images in mind, a 3D input will be processed with the dimensions of the image (32 by 32) as rows and columns and the 3 RGB channels as the depth. This results in an output dimension of \([32\times32\times3]\).

\subsubsection{Convolutional layer}

The convolutional layer is the main building block of a convolutional neural network. It is composed of neurons in a three dimensional volume. It  will compute the output of the neurons connected to local regions. Similar to standard neural networks it will compute the dot product of the neurons weight and the input, the only difference being that the input is a region instead of only one neuron. The output dimension will depend on the number of filters chosen, for example for 64 filters the dimensions will be \([32\times32\times64]\).

The parameters of the convolutional layer consist of filters. These filters will have a depth, which represents the depth of that convolutional layer and the amount of neurons the input will have to traverse. The width and height of the filter are usually small and represent the 2-dimensional filter which the input will traverse. As the input slides through the filter, the dot product is calculated. These filters will learn to activate when they see a specific feature at some spatial position of the input.

\begin{figure}[H]
    \centering
        \includegraphics[width=60mm]{Images/depth_cnn.jpeg}
        \caption{Input (red) goint into convolutional neural layer (blue) \cite{stanfordCNN}.}
\end{figure}

As opposed to standard artificial neural network layers, neurons of convolutional layers do not connect to all neurons of the neighbouring layers. When dealing with a very large input, for example images, it would make the model too complex if the layers were fully connected. The neurons will be connected only to a local region of the input. Their dimensions are usually very small, only covering a few pixels of the image. The depth will be the same as the one of the input, in this case it is the number of channels of the image. The spatial extend that is being ``looked at'' by the filters is referred to as the receptive field.

The depth of the convolutional layer, the number of filters the input has to traverse, will also be the size of its output. Each neuron along the depth will learn different features.

The stride of the convolutional layer is the parameter that regulates at which intervals each filter gets applied. If we have a stride of 1, then each filter would be right next to each other and their receptive fields, if greater than one, would overlap.

The use of zero padding along the edges of the input helps keeping its special dimensions after traversing the convolutional layer. As by going through the receptive fields, these cannot touch the outer surface of the image and therefore the borders would have to be discarded. It means that after each pass through a convolutional layer, the borders of the image would be slowly lost. It also makes designing the whole network easier if we do not have to worry about a small downsizing happening after each convolutional layer.

\subsubsection{ReLU layer}

The rectified linear unit (ReLU) layer is an activation function on the input element. A commonly used ReLU function is the \(max(0, x)\) which eliminates all negative values, leaving them at 0 and is linear for all positive values. The dimensions of the input will stay unchanged.

\subsubsection{Pool layer}

The pool layer is useful to down sample the dimensions of the input, reducing the computation necessary for the following layers, as well as help reduce the effects of overfitting. It is usually applied using 2 by 2 non-overlapping regions (receptive fields) and only taking the maximum value. It usually uses a stride of 2, so the regions do not overlap, but are right next to each other. With a pool size of 2 by 2 the dimensions of the output will change to \([16\times16\times64]\), discarding exactly 75\% of the inputs activations.

\subsubsection{Fully connected layer}

The fully connected layer is used as the last layer in a convolutional neural network, it is the same 1-dimensional layer as used in standard ANNs. The computed output will give us the class scores, one number of the probability that the input represents one of the classes. In this project, using the CIFAR-10 dataset, we have 10 classes resulting in the output dimensions of \([1\times1\times10]\).

\subsubsection{Rules of thumb}

There are a number of rules of thumb for convolutional neural networks. A lot of research has been done recently on CNNs and a number of hyper parameters have shown to repeatedly give good results.

It is said that the input layer should have a size (number of filters) divisible by 2, CNNs for the CIFAR-10 dataset often use the sizes 32 or 64.

The convolutional layers should use small filters of 3 times 3 or 5 times 5 with a stride of 1. Also padding the input with a layer of zeros around the borders is crucial so that the convolutional layer does not alter its dimensions (height and width). Also, by using a stride of 1, the dimensions of the images stay the same. The only layer downsizing the input will be the pooling layer.

The max pooling operation is generally used after each two convolutional layers. It should be used with a receptive field of 2 times 2 and a stride of 2. This method discards 75\% of the inputs activations, it is the only layer downsizing the input.

\section{Learning algorithms}

\subsection{Loss function}

In order to quantify the “unhappiness” of the result of a neural network we need a loss function. This will aid the model optimise its performance, therefore it is important to choose the correct one to make the model learn properly.

A very popular loss function is the hinge loss used in the multiclass support vector machine (SVM). This function is set up so that it will try to make the loss value lower for the desired outcome than for the wrong classification.

\begin{equation}
	L_i = \sum_{j≠y_i} max(0, s_j - s_{y_i} + \Delta)
\end{equation}

Another very popular loss function is the cross-entropy loss used in the softmax classifier. It has the advantage over the SVM that the output is more readable, as it returns normalized probabilities for each class. It will also calculate the full loss over the whole dataset, which can be used for validation. Its mathematical notation is the following:

\begin{equation}
	L_i = -f_{y_i} + log\sum_j e^{f_j}
\end{equation}

\subsection{Backpropagation}

Deep neural networks can have many hidden layers, all of which as opposed to the input and output layers do not get values provided. Backpropagation is a training algorithm for feedforward neural networks, that is able to compute values for each neuron locally. It is the most widely used training algorithm for these types of neural networks.

The algorithm is able to calculate its output value as well as the local gradient of its inputs with respect to its output value. It does so in two main phases:

\begin{itemize}
    \item Initially the output activation of the current neural network is computed by traversing through it from front to back. Once the output is calculated it will traverse the network backwards using the target that we want to reach. This generates the difference between the current hidden neurons of the network and the desired output.
    \item Now that it is visiting each neuron, it will multiply its output weight difference to the input activation to calculate the local gradient. Finally, it will correct the weights of each neuron by a ratio of the resulting local gradient.
\end{itemize}

The ratio at which the weights are corrected is referred to as the learning rate. The lower the learning rate, the slower the network learns and the slower it reaches convergence.

If the learning rate is too high, it can lead to divergence. This means that the training algorithm has failed to find a local or global minimum, usually by overshooting the target.

Usually a decay is used for the learning rate, it will start off relatively high and after some iterations slowly decrease as it closes in to the target.

\begin{figure}[H]
    \centering
        \includegraphics[width=100mm]{Images/learning_rate.png}
        \caption{High learning rate (left) compared to low learning rate (right).}
\end{figure}

As we can see in the graphs above, when the learning rate is very low it could end up getting stuck in a local minimum. Momentum is a helpful technique that makes it possible to jump out of the local minimum. This can be critical as the training algorithm would otherwise assume convergence and stop.

It is therefore important to tweak the learning rate, the decay and the momentum to make sure the algorithm finds convergence as fast as possible without overshooting it.

\section{Overfitting}

Deep neural networks can be made up of many layers of neurons, with each layer concentrating on a small portion of the whole classification. These systems can become very complex, learning things from the provided data that they should not. Overfitting is the term used for a neural network that fits the training data perfectly, it has learned every small little aspect from it but it can’t apply it in the real world.

Data never seen before by the network might look somewhat different to what the network might have gotten “used to”. Therefore, it is important to keep the features learned by the network more general, so it can apply its knowledge to other data that might look a little different but still be the same object.

Overfitting tends to occur when there is not enough diversity in the training data. The network will quickly learn little diversity provided and start concentrating on smaller details that are irrelevant to the actual problem.

\subsection{Early stopping}

The simplest way of stopping a network from overfitting is to monitor the validation performance and stop training the network as soon as it starts getting worse. It is also common to leave the network training for a certain amount of iterations and see if the validation performance improves again. If no better performing model is achieved, then the training stops and the old weights of the best performing model get loaded again.

\subsection{L2 regularization}

The L2 regularization a used frequently to solve the problem of overfitting. It can be seen as a penalization of weights that stand out a lot preferring flatter weights. It does so by applying the squared magnitude to the weights and multiplying it with the regularization strength, which makes weights that stand out, stand out even more.

One big advantage of this regularization is that it has the effect that it encourages networks to use many inputs a little, rather than some of the inputs a lot.

\subsection{L1 regularization}

The L1 regularization is quite similar to the L2 regularization, both of which can be combined. It has the effect of making the weights vectors become sparse during optimization. This means that the neurons will use only a very small amount of their most important inputs and therefore being less affected by strong inputs.

\subsection{Dropout}

Dropout is a very popular method to diminish overfitting, it has become pretty much imperative to deep neural networks \cite{citeulike:11835204}. The concept is simple, but has proven to work well. As the name implies, it drops out a random set of activations between neurons within a certain layer. A percentage is set so that in each iteration this certain amount of activations is dropped.

The idea is that the connection of the neurons will be taken away, making the neuron unable to reply on the presence of another neuron. The neuron will be forced to become useful on its own and work together with unknown others.

\section{Performance measures}

The evaluation of the neural network is albeit one of the most important aspects in this project. There are many ways to evaluate a model, the classification accuracy alone is not enough but a good starting point \cite{citeulike:5317666}. It will give us the percentage of correct predictions made for each class. The mathematical notation is the following:

\begin{equation}
	Accuracy = \frac{TruePos + TrueNeg}{TruePos + TrueNeg + FalsePos + FalseNeg}
\end{equation}

\subsection{Confusion matrix}

The confusion matrix is a very clean way to present the prediction results of the classifier compared to the target classification. It will display a table with the different classes on rows and columns. The predicted values will be shown in the columns and the target values in the rows.

It works by adding up how many classes were classified in what way. If a class was classified for example as class 0, it will go in the column 0 and if the target value was also 0 it will go in row 0. Each cell will contain the sum of all the predictions made in that particular combination.

The best possible result with a confusion matrix would be if the diagonal line, going from the top left down to the bottom right contained all the classifications.

\subsection{Precision}

The precision is the number of true positives, divided by the number of true positives and false positives. In a multi-class classification problem. This means that the predictions that were made for this class and are correct are divided by the total number of predictions that were made for this class, including all the predictions that are wrong. Essentially, the precision will tell us how exact the classifier is able make predictions. The mathematical notation is the following:

\begin{equation}
	Precision = \frac{TruePos}{TruePos + FalsePos}
\end{equation}

\subsection{Recall}

The recall is very similar to the precision in that it is the number of true positives, divided by the number of true positives and the number of false negatives. In this case we do not divide by the total predictions made for the current class, but by the total number of objects in the current class. The mathematical notation is the following:

\begin{equation}
	Recall = \frac{TruePos}{TruePos + FalseNeg}
\end{equation}

\subsection{F1-score}

The f1-score depends on the two previous evaluation measurements. It is a method to convey the balance between the precision and the recall. The mathematical notation is the following:

\begin{equation}
	F_1 score = 2 \times \frac{Precision \times Recall}{Precision + Recall}
\end{equation}

\section{Cross validation}

Cross validation splits the training set (usually into two) into a number of sets to train and validate the model on. The neural network will be trained on one of the sets and validate using the other set. Using a different set for validation helps against overfitting, from using always the same data.

k-fold cross validation is one of the most common methods used, the dataset is split into ``k equal'' subsets. The training is done on k-1 datasets, leaving one of the sets for validation.
