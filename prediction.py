
import argparse
import class_types as ct
import matplotlib.pyplot as plt
import model_handle
import numpy as np
from scipy import misc
from skimage import io

MODEL_NAME = "cnn_large"

parser = argparse.ArgumentParser()
parser.add_argument("--model", help="model to be used for the prediction.")
parser.add_argument("--image", help="image to be classified.")
parser.add_argument("--demo", help="run demo with all classes.")
args = parser.parse_args()


def load_image(image_file):
    image_full = io.imread(image_file)
    image = misc.imresize(image_full, (32, 32))
    image = image.transpose(2, 0, 1)
    image = np.expand_dims(image, axis=0)
    return image


def predict(model, model_name, image):
    # Reshape the data to 1 dimension for MLP
    if model_name == "mlp":
        image = image.reshape(image.shape[0],
                              (image.shape[1]
                               * image.shape[2]
                               * image.shape[3]))

    prediction = model.predict_classes(image, verbose=0)
    prediction = ct.class_names[prediction[0]]
    probabilities = model.predict(image, verbose=0)
    print("\nClassified as: ", prediction)
    print("Probability: ", probabilities)
    fig = plt.figure()
    plot_image(image, fig)
    plot_class_probabilities(probabilities, prediction, fig)
    plt.show()


def prediction_demo(model, model_name, image_class="ship"):
    folder = "data/test_images/" + image_class + "/"

    for x in range(1, 9):
        image = load_image(folder + str(x) + ".jpg")
        predict(model, model_name, image)


def plot_class_probabilities(probabilities, prediction, fig):
    fig.add_subplot(1, 2, 2)
    y = probabilities.reshape(probabilities.shape[1])
    N = len(y)
    x = range(N)
    width = 0.8
    plt.bar(x, y, width, color="blue")
    tick_marks = np.arange(len(ct.class_names))
    plt.xticks(tick_marks, ct.class_names, rotation=45)
    plt.title(prediction, fontsize=40)


def plot_image(image, fig):
    fig.add_subplot(1, 2, 1)
    image = image.reshape(3, 32, 32)
    image = image.transpose(1, 2, 0)
    plt.imshow(image)


if __name__ == "__main__":
    if args.model:
        model_name = args.model
    else:
        model_name = MODEL_NAME

    model = model_handle.load_model(model_name)

    if args.image:
        image = load_image(args.image)
        print(image.shape)
        predict(model, model_name, image)

    if args.demo:
        prediction_demo(model, model_name, args.demo)
