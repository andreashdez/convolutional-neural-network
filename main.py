"""
Convolutional Neural Network using CIFAR10 training dataset.

file name: main.py
author: Andreas Hernandez Hauser (anh45@aber.ac.uk)
description: A convolutional neural network and multilayer perceptron
             build using the keras deep learning framework
             and trained on the CIFAR10 dataset.
"""
import data_prep
import evaluation
import importlib as imp
import json
import model_handle
import training
import prediction


MODEL_NAME = "cnn_large"
BATCH_SIZE = 64
NB_EPOCH = 50
GREYSCALE = False
DATA_AUGMENTATION = True
SAVE_MODEL = False
LOAD_MODEL = True
TRAIN = False
EVALUATE = True
PREDICT = True


def save_history(history):
    with open("history.txt", "w") as outfile:
        json.dump(history, outfile)


if __name__ == "__main__":

    if TRAIN or EVALUATE:
        dataset = data_prep.load_data(MODEL_NAME, GREYSCALE)

    if LOAD_MODEL:
        model = model_handle.load_model(MODEL_NAME)
    else:
        imported_model = imp.import_module("models." + MODEL_NAME)
        model = imported_model.create_nn(dataset)
        if SAVE_MODEL:
            model_handle.save_model(model, MODEL_NAME)

    if TRAIN:
        model, hist = training.train_model(model, dataset, MODEL_NAME,
                                           DATA_AUGMENTATION,
                                           BATCH_SIZE, NB_EPOCH)
        evaluation.plot_learning_rate(hist)
        save_history(hist.history)

    if EVALUATE:
        evaluation.evaluate_model(model, dataset)

    if PREDICT:
        prediction.prediction_demo(model, MODEL_NAME)
