
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.layers.core import Activation, Dense, Dropout, Flatten
from keras.models import Sequential
from keras.optimizers import SGD


def create_nn(dataset):
    """Create convolutional neural network model."""
    model = Sequential()

    model.add(Convolution2D(64, 3, 3, border_mode="same",
                            input_shape=(dataset.img_channels,
                                         dataset.img_rows, dataset.img_cols)))
    model.add(Activation("relu"))
    model.add(Convolution2D(64, 3, 3))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.3))

    model.add(Convolution2D(128, 3, 3, border_mode="same"))
    model.add(Activation("relu"))
    model.add(Convolution2D(128, 3, 3))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.3))

    model.add(Convolution2D(256, 3, 3, border_mode="same"))
    model.add(Activation("relu"))
    model.add(Convolution2D(256, 3, 3))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.4))

    model.add(Flatten())
    model.add(Dense(256))
    model.add(Activation("relu"))
    model.add(Dropout(0.5))
    model.add(Dense(128))
    model.add(Activation("relu"))
    model.add(Dropout(0.5))
    model.add(Dense(dataset.nb_classes))
    model.add(Activation("softmax"))

    model.compile(loss="categorical_crossentropy", metrics=["accuracy"],
                  optimizer=SGD(lr=0.01, decay=1e-6,
                                momentum=0.9, nesterov=True))
    return model
