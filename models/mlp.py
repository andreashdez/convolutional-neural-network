
from keras.layers.core import Activation, Dense, Dropout
from keras.models import Sequential
from keras.optimizers import SGD


def create_nn(dataset):
    """Create convolutional neural network model."""
    model = Sequential()

    model.add(Dense(512, input_shape=(dataset.train_data.shape[1], )))
    model.add(Activation("relu"))
    model.add(Dropout(0.2))

    model.add(Dense(256))
    model.add(Activation("relu"))
    model.add(Dropout(0.2))

    model.add(Dense(128))
    model.add(Activation("relu"))
    model.add(Dropout(0.2))

    model.add(Dense(dataset.nb_classes))
    model.add(Activation("softmax"))

    model.compile(loss="categorical_crossentropy", metrics=["accuracy"],
                  optimizer=SGD(lr=0.01, decay=1e-6,
                                momentum=0.9, nesterov=True))
    return model
